<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class UsersController extends Controller
{
    /**
     * Поиск пользователей
     *
     * @param Request $request
     * @param Client $client
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, Client $client)
    {
        $this->validate($request, [
            'name' => 'required|string',
        ]);

//        return User::where('name', 'like', "$request->name%")
//                   ->limit(5)
//                   ->pluck('name');

        $contents = $client->get('https://api.github.com/search/users?q=' . $request->name)
                           ->getBody()
                           ->getContents();

        $users = json_decode($contents, true) ['items'];

        return response()->json(array_column($users, 'login'));
    }
}
