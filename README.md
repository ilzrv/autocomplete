# Installation

### Clone the repository

```php
git clone git@gitlab.com:ilzrv/autocomplete.git
```

### Install PHP Dependencies

```php
composer install
```

### Create .env

```php
cp .env.example .env
```

### Set Application Key

```php
php artisan key:generate
```

### Install NPM Dependencies

```php
npm install
```

### Build NPM Packages
```php
npm run production
```
